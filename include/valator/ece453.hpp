/*
 */

#ifndef __ECE453_APP_H__
#define __ECE453_APP_H__

#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>

#define GPIO_IN_REG         "/sys/kernel/ece453/gpio_in"
#define GPIO_OUT_REG        "/sys/kernel/ece453/gpio_out"
#define IM_REG              "/sys/kernel/ece453/interrupt_mask"
#define IRQ_REG             "/sys/kernel/ece453/irq"
#define PID_REG             "/sys/kernel/ece453/pid"
#define DUTY_CYC_M0_REG     "/sys/kernel/ece453/duty_cyc_m0"
#define DUTY_CYC_M1_REG     "/sys/kernel/ece453/duty_cyc_m1"
#define DUTY_CYC_M2_REG     "/sys/kernel/ece453/duty_cyc_m2"
#define DUTY_CYC_M3_REG     "/sys/kernel/ece453/duty_cyc_m3"
#define POSITION_S0_REG     "/sys/kernel/ece453/position_s0"
#define POSITION_S1_REG     "/sys/kernel/ece453/position_s1"
#define ENC_COUNT_M0_REG    "/sys/kernel/ece453/enc_count_m0"
#define ENC_COUNT_M1_REG    "/sys/kernel/ece453/enc_count_m1"
#define ENC_COUNT_M2_REG    "/sys/kernel/ece453/enc_count_m2"
#define ENC_COUNT_M3_REG    "/sys/kernel/ece453/enc_count_m3"

//*******************************************************************
// Register Addresses
//*******************************************************************
#define ECE453_IM_OFFSET            0
#define ECE453_IRQ_OFFSET           1
#define ECE453_GPIO_IN_OFFSET       2
#define ECE453_GPIO_OUT_OFFSET      3
#define ECE453_DUTY_CYC_M0_OFFSET   4
#define ECE453_DUTY_CYC_M1_OFFSET   5
#define ECE453_DUTY_CYC_M2_OFFSET   6
#define ECE453_DUTY_CYC_M3_OFFSET   7
#define ECE453_POSITION_S0_OFFSET   8
#define ECE453_POSITION_S1_OFFSET   9
#define ECE453_ENC_COUNT_M0_OFFSET  10
#define ECE453_ENC_COUNT_M1_OFFSET  11
#define ECE453_ENC_COUNT_M2_OFFSET  12
#define ECE453_ENC_COUNT_M3_OFFSET  13

//*******************************************************************
// Register Bit definitions
//*******************************************************************
#define REVERSE_MOTOR_BIT_NUM             8
#define REVERSE_MOTOR_BIT_MASK            (0x1 << REVERSE_MOTOR_BIT_NUM)

//*****************************************************************************
//*****************************************************************************
int ece453_reg_read(char const *reg_name);

//*****************************************************************************
//*****************************************************************************
int ece453_reg_write(char const *reg_name, int value);

#endif
