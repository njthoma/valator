#ifndef __MOTOR_CONTROL_H__
#define __MOTOR_CONTROL_H__

#include "registers.hpp"

#define  CM_PER_COUNT  0.0032717
#define  MAX_CURRENT   0.386
#define  MAX_DUTY_CYC  255

int motor = 0;
int encoder = 0;
float cmd_vel = 0;
bool reverse = false;
float Kp = 0;
float Ki = 0;
float accumulated_error = 0;
float period = 0.001;

float counts_to_vel(int counts);

float pid(float error);

int convert_duty_cyc(float pi_var);

#endif
