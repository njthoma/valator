#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>
#include <sstream>
#include "valator/servo_controller.hpp"
#include "ros/ros.h"
#include "std_msgs/Float32.h"

#define PID "/sys/kernel/ece453/pid"

//*****************************************************************************
//*****************************************************************************
int set_pid(void)
{
    char buf[10];
    int fd = open(PID, O_WRONLY);
    if(fd < 0) {
        perror("open");
        return -1;
    }
    sprintf(buf, "%i", getpid());
    if (write(fd, buf, strlen(buf) + 1) < 0) {
        perror("fwrite"); 
        return -1;
    }
    close(fd);
    return 0;
}

//*****************************************************************************
//*****************************************************************************
int clear_pid(void)
{
    char buf[10];
    int fd = open(PID, O_WRONLY);
    if(fd < 0) {
        perror("open");
        return -1;
    }
    memset(buf,0,10);
    if (write(fd, buf, strlen(buf) + 1) < 0) {
        perror("fwrite"); 
        return -1;
    }
    close(fd);
    return 0;
}

//*****************************************************************************
//*****************************************************************************
void mySigintHandler(int sig) {
    set_angle(servo, 110);
    ros::Duration(1).sleep();
    clear_pid();
    ros::shutdown();
}

//*****************************************************************************
//*****************************************************************************
void cmd_angle_cb(const std_msgs::Float32::ConstPtr& msg) {
    int angle = (int) msg->data;
    if (angle > 255) {
        angle = 255;
    }
    else if (angle < 0) {
        angle = 0;
    }
    cmd_angle = angle;
    ROS_INFO("set servo %d angle: [%d]", servo, angle);
}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv)
{
    // Configure the IP module
    set_pid();

    // Access private namespace
    ros::init(argc, argv, "servo_controller");
    ros::NodeHandle n("~");

    // Override the default ros sigint handler.
    // This must be set after the first NodeHandle is created.
    signal(SIGINT, mySigintHandler);

    // Get servo ID
    n.getParam("servo_id", servo);
    ROS_INFO("servo_id = [%d]", servo);

    // Setup subscriber to servo angle topic
    ros::Subscriber sub = n.subscribe("cmd_angle", 1000, cmd_angle_cb);

    // Set loop rate 1KHz
    ros::Rate loop_rate(1000);

    while(ros::ok()) {
        // Set angle
        set_angle(servo, cmd_angle);

        // Check for new messages
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
