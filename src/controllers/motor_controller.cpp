#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>
#include <sstream>
#include "valator/motor_controller.hpp"
#include "ros/ros.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int32.h"

#define PID "/sys/kernel/ece453/pid"

//*****************************************************************************
//*****************************************************************************
int set_pid(void)
{
    char buf[10];
    int fd = open(PID, O_WRONLY);
    if(fd < 0) {
        perror("open");
        return -1;
    }
    sprintf(buf, "%i", getpid());
    if (write(fd, buf, strlen(buf) + 1) < 0) {
        perror("fwrite"); 
        return -1;
    }
    close(fd);
    return 0;
}

//*****************************************************************************
//*****************************************************************************
int clear_pid(void)
{
    char buf[10];
    int fd = open(PID, O_WRONLY);
    if(fd < 0) {
        perror("open");
        return -1;
    }
    memset(buf,0,10);
    if (write(fd, buf, strlen(buf) + 1) < 0) {
        perror("fwrite"); 
        return -1;
    }
    close(fd);
    return 0;
}

//*****************************************************************************
//*****************************************************************************
void mySigintHandler(int sig) {
    set_duty_cyc(motor, 0, reverse);
    ros::Duration(1).sleep();
    clear_pid();
    ros::shutdown();
}

//*****************************************************************************
//*****************************************************************************
float counts_to_vel(int counts) {
    return (float)((float)counts * CM_PER_COUNT) / (float)period;
}

//*****************************************************************************
//*****************************************************************************
int convert_duty_cyc(float pi_var) {
    int duty_cyc = (int) pi_var;
    if (duty_cyc < 0) {
        duty_cyc = -1*duty_cyc;
        reverse = true;
    }
    else {
        reverse = false;
    }

    if (duty_cyc > 255) duty_cyc = 255;
    else if (duty_cyc < 20) duty_cyc = 0;

    return duty_cyc;
}

//*****************************************************************************
//*****************************************************************************
float pi_next(float error) {
//    ROS_INFO("error = [%f]", error);
    float pos_windup_lim = 90;
    float neg_windup_lim = -90;

    // calculate integral term and suppress windup
    accumulated_error += error*period;
    if (accumulated_error > pos_windup_lim) {
        accumulated_error = pos_windup_lim;
    }
    else if (accumulated_error < neg_windup_lim) {
        accumulated_error = neg_windup_lim;
    }

    return Kp*error + Ki*accumulated_error;
}

//*****************************************************************************
//*****************************************************************************
void cmd_vel_cb(const std_msgs::Float32::ConstPtr& msg) {
    cmd_vel = msg->data;
    accumulated_error = 0;
//    ROS_INFO("set motor %d velocity: [%f]", motor, msg->data);
}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv)
{
    int new_counts = 0;
    int prev_counts = 0;
    int duty_cyc = 0;
    float meas_vel = 0;
    float pi_var = 0;

    // Configure the IP module
    set_pid();

    // Access private namespace
    ros::init(argc, argv, "motor_controller", ros::init_options::NoSigintHandler);
    ros::NodeHandle n("~");

    // Override the default ros sigint handler.
    // This must be set after the first NodeHandle is created.
    signal(SIGINT, mySigintHandler);

    // Get motor and encoder IDs
    n.getParam("motor_id", motor);
    n.getParam("encoder_id", encoder);
    //ROS_INFO("motor_id = [%d], encoder_id = [%d]", motor, encoder);

    // Get Kp and Ki values
    n.getParam("Kp", Kp);
    n.getParam("Ki", Ki);
    //ROS_INFO("Kp = [%f], Ki = [%f]", Kp, Ki);

    // Setup subscriber to motor velocity topic
    ros::Subscriber cmd_vel_sub = n.subscribe("cmd_vel", 1/period, cmd_vel_cb);

    // Set loop rate 1KHz
    ros::Rate loop_rate(1000);

    while(ros::ok()) {
        // Get encoder counts
        new_counts = get_enc_counts(encoder);

        // Calculate measured velocity
        meas_vel = counts_to_vel(new_counts-prev_counts);
        prev_counts = new_counts;

        // PI loop
        pi_var = pi_next(cmd_vel-meas_vel);

        // Calculate new duty cycle
        duty_cyc = convert_duty_cyc(pi_var);

        // Set new duty cycle
        set_duty_cyc(motor, duty_cyc, reverse);

        // Check for new messages
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
