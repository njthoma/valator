#include <string>
#include <iostream>
#include <sstream>
#include "ros/ros.h"
#include "std_msgs/Int32.h"

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv)
{
    // init rosnode
    ros::init(argc, argv, "test_pub");
    ros::NodeHandle n;

    ros::Publisher pub = n.advertise<std_msgs::Int32>("cmd_vel", 1000);

    ros::Rate loop_rate(10);

    int count = 0;
    while (ros::ok()) {
        std_msgs::Int32 msg;
        std::string input;

        std::cout << "enter velocity: \n";
        getline(std::cin, input);

        msg.data = std::atof(input.c_str());

        pub.publish(msg);

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
