#include <string>
#include <iostream>
#include <sstream>
#include "ros/ros.h"
#include "std_msgs/Float32.h"

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv)
{
    // init rosnode
    ros::init(argc, argv, "test_pub");
    ros::NodeHandle n;

    ros::Publisher pub_m0 = n.advertise<std_msgs::Float32>("/motor_front_left/cmd_vel", 1000);
    ros::Publisher pub_m1 = n.advertise<std_msgs::Float32>("/motor_front_right/cmd_vel", 1000);
    ros::Publisher pub_m2 = n.advertise<std_msgs::Float32>("/motor_rear_left/cmd_vel", 1000);
    ros::Publisher pub_m3 = n.advertise<std_msgs::Float32>("/motor_rear_right/cmd_vel", 1000);

    ros::Rate loop_rate(10);

    int count = 0;
    while (ros::ok()) {
        std_msgs::Float32 msg;
        std::string input;

        std::cout << "enter velocity: \n";
        getline(std::cin, input);

        msg.data = std::atof(input.c_str());

        pub_m0.publish(msg);
        pub_m1.publish(msg);
        pub_m2.publish(msg);
        pub_m3.publish(msg);

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
