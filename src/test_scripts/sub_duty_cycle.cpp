#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>
#include <sstream>
#include "valator/ece453.hpp"
#include "valator/registers.hpp"
#include "ros/ros.h"
#include "std_msgs/Int32.h"

#define PID "/sys/kernel/ece453/pid"

//*****************************************************************************
//*****************************************************************************
int set_pid(void)
{
    char buf[10];
    int fd = open(PID, O_WRONLY);
    if(fd < 0) {
        perror("open");
        return -1;
    }
    sprintf(buf, "%i", getpid());
    if (write(fd, buf, strlen(buf) + 1) < 0) {
        perror("fwrite"); 
        return -1;
    }
    close(fd);
    return 0;
}

//*****************************************************************************
//*****************************************************************************
int clear_pid(void)
{
    char buf[10];
    int fd = open(PID, O_WRONLY);
    if(fd < 0) {
        perror("open");
        return -1;
    }
    memset(buf,0,10);
    if (write(fd, buf, strlen(buf) + 1) < 0) {
        perror("fwrite"); 
        return -1;
    }
    close(fd);
    return 0;
}

//*****************************************************************************
//*****************************************************************************
void control_c_handler(int n, siginfo_t *info, void *unused)
{
    clear_pid();
    ece453_reg_write(IM_REG, 0);
}

//*****************************************************************************
//*****************************************************************************
void duty_cyc_cb(const std_msgs::Int32::ConstPtr& msg) {
    int duty_cyc = msg->data;
    if (duty_cyc < 0) {
        set_duty_cyc(MOTOR_FL, -1*duty_cyc, true);
        set_duty_cyc(MOTOR_FR, -1*duty_cyc, true);
        set_duty_cyc(MOTOR_RL, -1*duty_cyc, true);
        set_duty_cyc(MOTOR_RR, -1*duty_cyc, true);
    }
    else {
        set_duty_cyc(MOTOR_FL, duty_cyc, false);
        set_duty_cyc(MOTOR_FR, duty_cyc, false);
        set_duty_cyc(MOTOR_RL, duty_cyc, false);
        set_duty_cyc(MOTOR_RR, duty_cyc, false);
    }
    ROS_INFO("set duty cycle: [%d]", msg->data);
}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv)
{
    struct sigaction ctrl_c_sig;

    // Set up handler for when the user presses CNTL-C to stop the application
    ctrl_c_sig.sa_sigaction = control_c_handler;
    ctrl_c_sig.sa_flags = SA_SIGINFO;
    sigaction(SIGINT, &ctrl_c_sig, NULL);

    // Configure the IP module
    set_pid();

    ros::init(argc, argv, "sub_duty_cycle");
    ros::NodeHandle n;

    ros::Subscriber sub = n.subscribe("cmd_vel", 1000, duty_cyc_cb);

    ros::Rate loop_rate(1000);

    while(ros::ok()) {
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
