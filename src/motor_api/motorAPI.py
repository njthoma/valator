#!/usr/bin/env python
import rospy
from valator.msg import xbox_360_controller_motorAPI
from valator.msg import openCV_motorAPI
from std_msgs.msg import Float32
from enum import Enum
from time import sleep

class Directions(Enum):
        FWD    = 0
        BACK   = 1
        RIGHT  = 2
        LEFT   = 3
        FWD_R  = 4
        FWD_L  = 5
        BACK_R = 6
        BACK_L = 7
        STOP   = 8
	SPIN_L = 9
	SPIN_R = 10

###############################################################################
#Handles open_cv input
def open_cv_callback(data):
	#Initialize variables
	
	#FWD/BACK velocity from PID loop
	y_vel = data.y_vel
	#LEFT/RIGHT velocity from PID loop
	x_vel = data.x_vel
	
	FL_cmd_vel = 0
	FR_cmd_vel = 0
	BL_cmd_vel = 0
	BR_cmd_vel = 0

	#Adjust cmd_vel based on directional input
	#STOP command
	if (y_vel == 0 and x_vel == 0):
		FL_cmd_vel = 0
		FR_cmd_vel = 0
		BL_cmd_vel = 0
		BR_cmd_vel = 0
	#FWD
	elif (y_vel > 0 and x_vel == 0):
		FL_cmd_vel = y_vel
		FR_cmd_vel = y_vel
		BL_cmd_vel = y_vel
		BR_cmd_vel = y_vel
	#BACK
	elif (y_vel < 0 and x_vel == 0):
		FL_cmd_vel = y_vel
		FR_cmd_vel = y_vel
		BL_cmd_vel = y_vel
		BR_cmd_vel = y_vel
	#RIGHT: left motors drive FWD, Right motors drive BACK
	elif (y_vel == 0 and x_vel < 0):
		FL_cmd_vel = -(x_vel)
		FR_cmd_vel = x_vel
		BL_cmd_vel = -(x_vel)
		BR_cmd_vel = x_vel
	#LEFT: left motors drive BACK, right motors drive FWD
	elif (y_vel == 0 and x_vel > 0):
		FL_cmd_vel = -(x_vel)
		FR_cmd_vel = x_vel
		BL_cmd_vel = -(x_vel)
		BR_cmd_vel = x_vel	
	#FWD_R: Reduce speed on right wheels
	elif (y_vel > 0 and x_vel < 0):
		FL_cmd_vel = y_vel
		FR_cmd_vel = max(0, y_vel + x_vel)
		BL_cmd_vel = y_vel
		BR_cmd_vel = max(0, y_vel + x_vel) 
	#FWD_L: Reduce speed on left wheels
	elif (y_vel > 0 and x_vel > 0):
		FL_cmd_vel = max(0, y_vel - x_vel)
		FR_cmd_vel = y_vel
		BL_cmd_vel = max(0, y_vel - x_vel)
		BR_cmd_vel = y_vel
	#BACK_R
	elif (y_vel < 0 and x_vel < 0):
		FL_cmd_vel = y_vel
		FR_cmd_vel = min(0, y_vel - x_vel)
		BL_cmd_vel = y_vel
		BR_cmd_vel = min(0, y_vel - x_vel)
	#BACK_L
	elif (y_vel < 0 and x_vel > 0):
		FL_cmd_vel = min(0, y_vel + x_vel)
		FR_cmd_vel = y_vel
		BL_cmd_vel = min(0, y_vel + x_vel)
		BR_cmd_vel = y_vel
	
	
		
	#Publish
	motor_front_left.publish(FL_cmd_vel)
	motor_front_right.publish(FR_cmd_vel)
	motor_back_left.publish(BL_cmd_vel)
	motor_back_right.publish(BR_cmd_vel)

		
		
###############################################################################			
#Handles xbox 360 controller input
def manual_callback(data):
	#Initialize variables
	cmd_vel        = data.velocity
	direction      = data.direction
	pan_servo_deg  = data.pan_servo_deg
	tilt_servo_deg = data.tilt_servo_deg
	turn_cmd_vel   = data.turn_velocity

	#Initialize motor cmd_vel, FL = front left, etc
	FL_cmd_vel = 0
	FR_cmd_vel = 0
	BL_cmd_vel = 0
	BR_cmd_vel = 0
	
	#Adjust cmd_vel based on directional input
	#STOP command
	if (direction == Directions.STOP.value):
		FL_cmd_vel = 0
		FR_cmd_vel = 0
		BL_cmd_vel = 0
		BR_cmd_vel = 0
	#FWD
	elif (direction == Directions.FWD.value):
		FL_cmd_vel = cmd_vel
		FR_cmd_vel = cmd_vel
		BL_cmd_vel = cmd_vel
		BR_cmd_vel = cmd_vel
	#BACK
	elif (direction == Directions.BACK.value):
		FL_cmd_vel = cmd_vel
		FR_cmd_vel = cmd_vel
		BL_cmd_vel = cmd_vel
		BR_cmd_vel = cmd_vel
	#RIGHT: left motors drive FWD, Right motors drive BACK
	elif (direction == Directions.RIGHT.value):
		FL_cmd_vel = turn_cmd_vel
		FR_cmd_vel = -(turn_cmd_vel)
		BL_cmd_vel = turn_cmd_vel
		BR_cmd_vel = -(turn_cmd_vel)
	#LEFT: left motors drive BACK, right motors drive FWD
	elif (direction == Directions.LEFT.value):
		FL_cmd_vel = -(turn_cmd_vel)
		FR_cmd_vel = turn_cmd_vel
		BL_cmd_vel = -(turn_cmd_vel)
		BR_cmd_vel = turn_cmd_vel
	#FWD_R: Reduce speed on right wheels
	elif (direction == Directions.FWD_R.value):
		FL_cmd_vel = cmd_vel
		FR_cmd_vel = max(0, cmd_vel - MOVING_TURN_DECREMENT)
		BL_cmd_vel = cmd_vel
		BR_cmd_vel = max(0, cmd_vel - MOVING_TURN_DECREMENT) 
	#FWD_L: Reduce speed on left wheels
	elif (direction == Directions.FWD_L.value):
		FL_cmd_vel = max(0, cmd_vel - MOVING_TURN_DECREMENT)
		FR_cmd_vel = cmd_vel
		BL_cmd_vel = max(0, cmd_vel - MOVING_TURN_DECREMENT)
		BR_cmd_vel = cmd_vel
	#BACK_R
	elif (direction == Directions.BACK_R.value):
		FL_cmd_vel = cmd_vel
		FR_cmd_vel = min(0, cmd_vel + MOVING_TURN_DECREMENT)
		BL_cmd_vel = cmd_vel
		BR_cmd_vel = min(0, cmd_vel + MOVING_TURN_DECREMENT)
	#BACK_L
	elif (direction == Directions.BACK_L.value):
		FL_cmd_vel = min(0, cmd_vel + MOVING_TURN_DECREMENT)
		FR_cmd_vel = cmd_vel
		BL_cmd_vel = min(0, cmd_vel + MOVING_TURN_DECREMENT)
		BR_cmd_vel = cmd_vel
	#SPIN_L	
	elif (direction == Directions.SPIN_L.value):
		FL_cmd_vel = -(SPIN_SPEED)
		FR_cmd_vel = SPIN_SPEED
		BL_cmd_vel = -(SPIN_SPEED)
		BR_cmd_vel = SPIN_SPEED
	
		motor_front_left.publish(FL_cmd_vel)
		motor_front_right.publish(FR_cmd_vel)
		motor_back_left.publish(BL_cmd_vel)
		motor_back_right.publish(BR_cmd_vel)
			
		#Spin for time
		spin_time = 165.0 / SPIN_SPEED
		sleep(spin_time)		
			
		FL_cmd_vel = 0
		FR_cmd_vel = 0
		BL_cmd_vel = 0
		BR_cmd_vel = 0
			
		
	#print "FR_cmd_vel = {}".format(FR_cmd_vel)
	#print "FL_cmd_vel = {}".format(FL_cmd_vel)
	#print "BR_cmd_vel = {}".format(BR_cmd_vel)
	#print "BL_cmd_vel = {}".format(BL_cmd_vel)
	#print Directions(direction)
	
	#Publish
	motor_front_left.publish(FL_cmd_vel)
	motor_front_right.publish(FR_cmd_vel)
	motor_back_left.publish(BL_cmd_vel)
	motor_back_right.publish(BR_cmd_vel)
	
	#Set pan angle
	servo_pan.publish(data.pan_servo_deg)
	servo_tilt.publish(data.tilt_servo_deg)	
#END MANUAL CALLBACK	
###############################################################################
def listener():
	#Setup motorAPI node
	rospy.init_node('motorAPI', anonymous=True)
	
	#Setup xbox subscriber
	rospy.Subscriber('xbox_360_controller_motorAPI',
			 xbox_360_controller_motorAPI,
			 manual_callback,
			 queue_size = 100)
	
	rospy.Subscriber('openCV_motorAPI',
			openCV_motorAPI,
			open_cv_callback,
			queue_size = 100)
	
	#Initialize motors to 0			
	motor_front_left.publish(0)
	motor_front_right.publish(0)
	motor_back_left.publish(0)
	motor_back_right.publish(0)
	
	#Listen for new input
	rospy.spin()
	

###################################################################################	
#CONSTANTS
MOVING_TURN_DECREMENT         = 25 #cm/s
MOVING_TURN_DECREMENT_OPEN_CV = 10 #cm/s
SPIN_SPEED		      = 90
#END CONSTANTS

#Setup 4 motor publishers	
motor_front_left = rospy.Publisher('/motor_front_left/cmd_vel',
				   Float32,
				   queue_size = 10)	
	
motor_front_right = rospy.Publisher('/motor_front_right/cmd_vel',
				   Float32,
				   queue_size = 10)	
	
motor_back_left = rospy.Publisher('/motor_rear_left/cmd_vel',
				   Float32,
				   queue_size = 10)	
	
motor_back_right = rospy.Publisher('/motor_rear_right/cmd_vel',
				   Float32,
				   queue_size = 10)	

#Setup servo publishers
servo_tilt = rospy.Publisher('/servo_tilt/cmd_angle',
			    Float32,
			    queue_size = 10)	
	
servo_pan = rospy.Publisher('/servo_pan/cmd_angle',
			    Float32,
			    queue_size = 10)	



if __name__ == '__main__':
	listener()
	 
