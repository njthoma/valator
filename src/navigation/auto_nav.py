#!/usr/bin/env python

import sys, signal
import argparse
import cv2
import numpy as np
import imutils
import rospy
from control import PIController
from target import TargetObject
from stream import StreamThread
from colors import ObjectColors
from valator.msg import openCV_motorAPI, xbox_360_controller_openCV, k_values

class SigHandler:
    def __init__(self, cap):
        self.cap = cap

    def control_c_handler(self):
        self.cap.release()
        cv2.destroyAllWindows()

def get_ros_params():
    # target color
    if rospy.has_param('~color'):
        color_name = rospy.get_param('~color')
        if color_name == 'red':
            color = ObjectColors.RED.value
        elif color_name == 'blue':
            color = ObjectColors.BLUE.value
        elif color_name == 'yellow':
            color = ObjectColors.YELLOW.value
        elif color_name == 'green':
            color = ObjectColors.GREEN.value
        else:
            raise ValueError('color must one of [red, blue, yellow, green]')
    else:
        color = ObjectColors.RED.value
    # display boolean (must have X11 forwarding if True)
    if rospy.has_param('~display'):
        display = rospy.get_param('~display')
    else:
        display = True
    # target fraction of frame area
    if rospy.has_param('~area_mult'):
        area_mult = rospy.get_param('~area_mult')
    else:
        area_mult = 0.20
    # control loop rate
    if rospy.has_param('~period'):
        period = rospy.get_param('~period')
    else:
        period = 0.08
    # proportional gain for left/right pi loop
    if rospy.has_param('~Kp_x'):
        Kp_x = rospy.get_param('~Kp_x')
    else:
        Kp_x = 0.052
    # integral gain for left/right pi loop
    if rospy.has_param('~Ki_x'):
        Ki_x = rospy.get_param('~Ki_x')
    else:
        Ki_x = 0.0002
    # maximum velocity for left/right pi loop
    if rospy.has_param('~max_vel_x'):
        max_vel_x = rospy.get_param('~max_vel_x')
    else:
        max_vel_x = 34.25
    # proportional gain for fwd/rev pi loop
    if rospy.has_param('~Kp_y'):
        Kp_y = rospy.get_param('~Kp_y')
    else:
        Kp_y = 0.1
    # integral gain for fwd/rev pi loop
    if rospy.has_param('~Ki_y'):
        Ki_y = rospy.get_param('~Ki_y')
    else:
        Ki_y = 0.012
    # maximum velocity for fwd/rev pi loop
    if rospy.has_param('~max_vel_y'):
        max_vel_y = rospy.get_param('~max_vel_y')
    else:
        max_vel_y = 17.0
    return color, display, area_mult, period, Kp_x, Ki_x, max_vel_x, Kp_y, Ki_y, max_vel_y

def set_gains_sub(data, args):
    x_pi = args[0]
    y_pi = args[1]
    x_pi.Kp = data.kp_left_right
    x_pi.Ki = data.ki_left_right
    x_pi.max_vel = data.vel_left_right
    y_pi.Kp = data.kp_fwd_rev
    y_pi.Ki = data.ki_fwd_rev
    y_pi.max_vel = data.vel_fwd_rev
    rospy.loginfo("Kp_x = [%f], Ki_x = [%f], vel_x = [%f], Kp_y = [%f], Ki_y = [%f], vel_y = [%f]"%(x_pi.Kp, x_pi.Ki, x_pi.max_vel, y_pi.Kp, y_pi.Ki, y_pi.max_vel))

def set_target_sub(data, args):
    obj = args
    manual_mode = data.manual_mode
    color = data.openCV_color
    colors = [c.value for c in ObjectColors]
    #rospy.loginfo("color = %d, manual_mode = %d"%(color,manual_mode))
    if manual_mode:
        obj.disable_auto_nav()
    elif not manual_mode and color in colors:
        obj.enable_auto_nav()
        obj.set_new_target(color)

def main():
    rospy.init_node('auto_nav', anonymous=True)

    # Grab webcam
    cap = cv2.VideoCapture(0)

    # Kill handler
    sig = SigHandler(cap)
    rospy.on_shutdown(sig.control_c_handler)

    # Get rosparams
    color, display, area_mult, period, Kp_x, Ki_x, max_vel_x, Kp_y, Ki_y, max_vel_y = get_ros_params()

    # Setup PI control loops
    x_pi = PIController(period, Kp_x, Ki_x, max_vel_x)
    y_pi = PIController(period, Kp_y, Ki_y, max_vel_y)

    # Setup velocity publisher
    vel_pub = rospy.Publisher('openCV_motorAPI', openCV_motorAPI, queue_size=10)

    # Setup target
    obj = TargetObject(cap, display, color, area_mult, x_pi, y_pi, vel_pub)

    # Setup target subscriber
    rospy.Subscriber('xbox_360_controller_openCV', xbox_360_controller_openCV, set_target_sub, (obj))

    # Setup debug subscriber
    rospy.Subscriber('k_values', k_values, set_gains_sub, (x_pi, y_pi))

    # Message instances
    motorAPI_msg = openCV_motorAPI()

    # Loop rate
    r = rospy.Rate(1/period)

    # Debug
    #obj.enable_auto_nav()

    while not rospy.is_shutdown():
        if obj.enable:
            # Get target errors
            x_error, y_error = obj.get_target_error()

            # Step PI loop
            x_vel = x_pi.next(x_error)
            y_vel = y_pi.next(y_error)

            # Clip velocities
            x_vel_clip = x_pi.clip(x_vel)
            y_vel_clip = y_pi.clip(y_vel)

            # Publish new velocities
            motorAPI_msg.x_vel = x_vel_clip
            motorAPI_msg.y_vel = y_vel_clip
            vel_pub.publish(motorAPI_msg)
            #rospy.loginfo('x_vel = [%f], y_vel = [%f]'%(x_vel_clip, y_vel_clip))

            r.sleep()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
