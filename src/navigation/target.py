import cv2
import numpy as np
import imutils
import rospy
from colors import ObjectColors
from valator.msg import openCV_motorAPI

class TargetObject:
    def __init__(self, cap, display, color, area_mult, x_pi, y_pi, vel_pub):
        self.enable = False
        self.cap = cap
        self.display = display
        self.x_pi = x_pi
        self.y_pi = y_pi
        self.vel_pub = vel_pub
        self.motorAPI_msg = openCV_motorAPI()
        self.x_error = 0
        self.y_error = 0
        self.color = color
        self.upper, self.lower = self.rgb_bounds(color)
        self.frame_center = (350,263)
        self.frame_area  = area_mult * 700 * 525

    def enable_auto_nav(self):
        self.enable = True

    def disable_auto_nav(self):
        self.motorAPI_msg.x_vel = 0.0
        self.motorAPI_msg.y_vel = 0.0
        self.vel_pub.publish(self.motorAPI_msg)
        self.enable = False

    def rgb_bounds(self, color):
        if color == ObjectColors.RED.value:
            lower = np.array([0, 100, 100], dtype='uint8')
            upper = np.array([6, 255, 255], dtype='uint8')
        elif color == ObjectColors.BLUE.value:
            lower = np.array([90, 50, 50], dtype='uint8')
            upper = np.array([120, 255, 255], dtype='uint8')
        elif color == ObjectColors.YELLOW.value:
            lower = np.array([20, 50, 50], dtype='uint8')
            upper = np.array([40, 255, 255], dtype='uint8')
        elif color == ObjectColors.GREEN.value:
            lower = np.array([40, 30, 30], dtype='uint8')
            upper = np.array([80, 255, 255], dtype='uint8')
        else:
            raise ValueError('color must one of [red, blue, yellow, green]')
        return upper, lower

    def set_new_target(self, color):
        self.x_pi.clear_accumulated_error()
        self.y_pi.clear_accumulated_error()
        self.upper, self.lower = self.rgb_bounds(color)

    def get_target_error(self):
        _,frame = self.cap.read()
        frame = imutils.resize(frame, width=700)
		
	#Setting up crosshair for testing purposes.
	cv2.line(frame,(325,263),(375,263),(255,0,0),3)
	cv2.line(frame,(350,235),(350,285),(255,0,0),3)
	center_circle = cv2.circle(frame,(350,263),25,(255,0,0),3)

        # Convert BGR to HSV
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	# Masking image to detect specified color
        mask = cv2.inRange(hsv, self.lower, self.upper)

        # Bitwise AND mask and original image
        out = cv2.bitwise_and(frame, frame, mask=mask)
	
	# Convert to black and white
	h,s,v = cv2.split(out)
		
	con_image, contours, hierarchy = cv2.findContours(v,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
 	
	try: hierarchy = hierarchy[0]
        except: hierarchy = []

        self.x_error = 0
        self.y_error = 0
        for contour, hier in zip(contours,hierarchy):
            (x,y,w,h) = cv2.boundingRect(contour)
	    if w> 30 and h > 30:
 	       	cv2.rectangle(v, (x,y), (x+w, y+h) , (255,0,0), 2)
		cv2.rectangle(frame,(x,y),(x+w,y+h), (255,0,0), 2)

		target_center = (x + (w / 2),y + (h / 2))
                target_area = h * w

		cv2.circle(frame,target_center,5,(255,0,0),5) 
		self.x_error = self.frame_center[0] - target_center[0]
		self.y_error = self.frame_area - target_area

        if self.x_error == 0 and self.y_error == 0:
            self.x_pi.clear_accumulated_error()
            self.y_pi.clear_accumulated_error()

        # Display the resulting frame
        if (self.display):
	    cv2.imshow('feed',v)
            cv2.imshow('frame',frame)
            cv2.waitKey(1)

        return self.x_error, self.y_error

