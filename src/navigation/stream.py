import cv2
import rospy
from threading import Thread

class StreamThread:
    def __init__(self, cap):
        self.cap = cap
        self.started = False
        self.update = False

    def update(self):
        rospy.loginfo("test")
#        while True:
#            _, self.frame = self.cap.read()

    def start_thread(self):
        Thread(target=self.update).start()
        self.started = True
        return self

    def get_frame(self):
        return self.frame
