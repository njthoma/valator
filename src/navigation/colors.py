from enum import Enum

class ObjectColors(Enum):
    GREEN  = 6
    RED    = 7
    BLUE   = 8
    YELLOW = 9
