class PIController:
    def __init__(self, period, Kp, Ki, max_vel):
        self.period = period
        self.accumulated_error = 0
        self.Kp = Kp
        self.Ki = Ki
        self.max_vel = max_vel
        self.pos_windup_lim = max_vel
        self.neg_windup_lim = -1 * max_vel

    def next(self, error):
        self.accumulated_error = self.accumulated_error + error * self.period
        if self.accumulated_error > self.pos_windup_lim:
            self.accumulated_error = self.pos_windup_lim
        elif self.accumulated_error < self.neg_windup_lim:
            self.accumulated_error = self.neg_windup_lim
        return self.Kp * error + self.Ki * self.accumulated_error

    def clip(self, vel):
        if vel > self.max_vel:
            return self.max_vel
        elif vel < (-1 * self.max_vel):
            return (-1 * self.max_vel)
        else:
            return vel

    def clear_accumulated_error(self):
        self.accumulated_error = 0

