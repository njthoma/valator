#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>

extern "C" {
#include "valator/bq34z100.h"
}
int 
main(int argc, char * arg[]){
    uint8_t read_val = 0x00;
    bq34z100_open();
    sleep(1);

    read_val = bq34z100_read(0x00,0x01);
    bq34z100_close();

    return 0;
}
