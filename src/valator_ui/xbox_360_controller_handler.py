#!/usr/bin/env python
import xbox_360_controller_object
import rospy
import os
from valator.msg import xbox_360_controller_motorAPI
from valator.msg import xbox_360_controller_openCV
from valator.msg import k_values
from enum import Enum

#Enums for xbox signals
class XboxControl(Enum):
	R_ANALOG        = 2
	RT              = 4
	LT              = 5
	A               = 6
	B               = 7
	X               = 8
	Y               = 9
	L_BUMPER        = 10
	R_BUMPER        = 11
	START           = 13
	XBOX		= 14
	BACK		= 12
	R_THUMB		= 16
	DIRECTIONAL_PAD = 17

#Enums for robot movement directions
class Directions(Enum):
	FWD    = 0
	BACK   = 1
	RIGHT  = 2
	LEFT   = 3
	FWD_R  = 4
	FWD_L  = 5
	BACK_R = 6
	BACK_L = 7
	STOP   = 8
	SPIN_L = 9
	SPIN_R = 10

#Enums for openCV colors to move towards
class OpenCV_Colors(Enum):
	GREEN    = 6
	RED      = 7
	BLUE     = 8
	YELLOW   = 9

class DEBUG_MODES(Enum):
	K_FWD_REV    = 0
	K_LEFT_RIGHT = 1
	VELOCITY     = 2

# exit gracefully
def exit():
        xboxCont.stop()
        #print "bye!"

#Set the movement direction
def set_direction(y_cmd_vel, r_analog_pos):
	direction = Directions.STOP.value	

	#Stop
	if y_cmd_vel == 0 and r_analog_pos == 0 :
		direction = Directions.STOP.value
	#FWD
	elif y_cmd_vel > 0 and r_analog_pos == 0 :
		direction = Directions.FWD.value
	#BACK
	elif y_cmd_vel < 0 and r_analog_pos == 0 :
		direction = Directions.BACK.value
	#RIGHT	 	
	elif y_cmd_vel == 0 and r_analog_pos > 0 :
		direction = Directions.RIGHT.value
	#LEFT	 	
	elif y_cmd_vel == 0 and r_analog_pos < 0 :
		direction = Directions.LEFT.value
	#FWD_R	 	
	elif y_cmd_vel > 0 and r_analog_pos > 0 :
		direction = Directions.FWD_R.value
	#FWD_L 	 	
	elif y_cmd_vel > 0 and r_analog_pos < 0 :
		direction = Directions.FWD_L.value
	#BACK_R	 	
	elif y_cmd_vel < 0 and r_analog_pos > 0 :
		direction = Directions.BACK_R.value
	#BACK_L
	elif y_cmd_vel < 0 and r_analog_pos < 0 :
		direction = Directions.BACK_L.value	 	
			 	
	#print Directions(direction)
	
	#Set message direction parameter
	motorAPI_msg.direction = direction
	motorAPI_msg.turn_velocity = abs(r_analog_pos)
	

# handle controller input
# this function prints input values so you can easily add and/or change behavior
def handleInput(controlId, value):
        #print "Control id = {}, Value = {}".format(controlId, value)
	#test for push
       	
	#Variables declared below
	#Would never push globals to production, but it'll work here
	global manual_mode
	global y_cmd_vel
	global fwd_vel
	global back_vel
	global pan_servo_deg
	global tilt_servo_deg
	global direction
	global r_analog_pos
	global openCV_color	
	global send_it_mode
	global ki_fwd_rev 
	global kp_fwd_rev
	global ki_left_right
	global kp_left_right
	global fwd_rev_k_values
	global vel_fwd_rev
	global vel_left_right
	global debug_mode

	#BACK kills all rosnodes 
	if controlId == XboxControl.BACK.value :
		nodes = os.popen("rosnode list").readlines()

		for i in range(len(nodes)):
    			nodes[i] = nodes[i].replace("\n","")

		for node in nodes:
    			os.system("rosnode kill "+ node)

		exit() 
	
	#Choose openCV color
	#A=GREEN, B=RED, X=BLUE, Y=YELLOW
	elif ((controlId == XboxControl.A.value or 
	   controlId == XboxControl.B.value or
	   controlId == XboxControl.X.value or
	   controlId == XboxControl.Y.value) and
	   value and
	   not manual_mode) :   
                openCV_color = controlId
		openCV_msg.openCV_color = openCV_color
		openCV_msg.manual_mode = manual_mode
		openCV_pub.publish(openCV_msg)
		#print OpenCV_Colors(openCV_color)
	#Right analog stick controls turning
	elif controlId == XboxControl.R_ANALOG.value and manual_mode :
		r_analog_pos = value
		set_direction(y_cmd_vel, r_analog_pos)
		motorAPI_pub.publish(motorAPI_msg)	 	
	#RT will move robot FWD
	elif controlId == XboxControl.RT.value and manual_mode:   
		fwd_vel = value
		
		#SEND_IT_MODE increases speed
		if send_it_mode :
			y_cmd_vel = (fwd_vel - back_vel) * 2
		else :
			y_cmd_vel = fwd_vel - back_vel

		motorAPI_msg.velocity = y_cmd_vel
		set_direction(y_cmd_vel, r_analog_pos)
		motorAPI_pub.publish(motorAPI_msg)
		#print y_cmd_vel
	#LT will move robot BACK
	elif controlId == XboxControl.LT.value and manual_mode:
		back_vel = value
		y_cmd_vel = fwd_vel - back_vel
		motorAPI_msg.velocity = y_cmd_vel
		set_direction(y_cmd_vel, r_analog_pos)
		motorAPI_pub.publish(motorAPI_msg)
		#print y_cmd_vel
	#DIRECTIONAL PAD will control the pan and tilt motors
	elif (controlId == XboxControl.DIRECTIONAL_PAD.value and 
			  (value[0] or value[1]) and 
			  manual_mode):
		#Tilt up
		if   (value[1] == 1 and ((pan_servo_deg + SERVO_DEG_CHANGE) <= MAX_DEG))   :
			pan_servo_deg += SERVO_DEG_CHANGE
		#Tilt down
		elif (value[1] == -1 and ((pan_servo_deg - SERVO_DEG_CHANGE) >= MIN_DEG))  :
			pan_servo_deg -= SERVO_DEG_CHANGE
		#Pan Right
		elif (value[0] == 1 and ((tilt_servo_deg + SERVO_DEG_CHANGE) <= MAX_DEG))  :
			tilt_servo_deg += SERVO_DEG_CHANGE 
		#Pan Left
		elif (value[0] == -1 and ((tilt_servo_deg - SERVO_DEG_CHANGE) >= MIN_DEG)) :
			tilt_servo_deg -= SERVO_DEG_CHANGE
	
		motorAPI_msg.pan_servo_deg = pan_servo_deg
		motorAPI_msg.tilt_servo_deg = tilt_servo_deg
		motorAPI_msg.direction = Directions.STOP.value
		motorAPI_pub.publish(motorAPI_msg)

	#Right bumber spins 180
	elif controlId == XboxControl.R_BUMPER.value and value and manual_mode :
			direction = Directions.SPIN_R.value
	#Left bumper spins 180
	elif controlId == XboxControl.L_BUMPER.value and value and manual_mode:
			direction = Directions.SPIN_L.value
			motorAPI_msg.direction = direction
			motorAPI_pub.publish(motorAPI_msg)
	###########################Autonomous Controls##################################	
	#Right bumber controls fwd_rev_k_values
	elif controlId == XboxControl.R_BUMPER.value and value and not manual_mode :
			debug_mode = DEBUG_MODES.K_FWD_REV.value
	#Left bumber controls left right k values
	elif controlId == XboxControl.L_BUMPER.value and value and not manual_mode:
			debug_mode = DEBUG_MODES.K_LEFT_RIGHT.value
	elif controlId == XboxControl.R_THUMB.value and value :
			debug_mode = DEBUG_MODES.VELOCITY.value	
	#Adjust the kp and ki values
	elif (controlId == XboxControl.DIRECTIONAL_PAD.value and 
			  (value[0] or value[1]) and 
			  not manual_mode):
		#Increase Kp, right
		if   value[0] == 1 :
			if debug_mode == DEBUG_MODES.K_FWD_REV.value :
				kp_fwd_rev += KP_FWD_REV_INC
			elif debug_mode == DEBUG_MODES.K_LEFT_RIGHT.value :
				kp_left_right += KP_LEFT_RIGHT_INC
			elif debug_mode == DEBUG_MODES.VELOCITY.value :
				vel_left_right += VEL_INC		
		#Decrease Kp, left
		elif value[0] == -1 :
			if debug_mode == DEBUG_MODES.K_FWD_REV.value   :
				kp_fwd_rev -= KP_FWD_REV_INC
			elif debug_mode == DEBUG_MODES.K_LEFT_RIGHT.value    :
				kp_left_right -= KP_LEFT_RIGHT_INC
			elif debug_mode == DEBUG_MODES.VELOCITY.value :
				vel_left_right -= VEL_INC		
		#Decrease Ki
		elif value[1] == 1 :
			if debug_mode == DEBUG_MODES.K_FWD_REV.value   :
				ki_fwd_rev += KI_FWD_REV_INC
			elif debug_mode == DEBUG_MODES.K_LEFT_RIGHT.value   :
				ki_left_right += KI_LEFT_RIGHT_INC
			elif debug_mode == DEBUG_MODES.VELOCITY.value :
				vel_fwd_rev += VEL_INC		 
		#Decrease Ki
		elif value[1] == -1 :
			if debug_mode == DEBUG_MODES.K_FWD_REV.value   :
				ki_fwd_rev -= KI_FWD_REV_INC
			elif debug_mode == DEBUG_MODES.K_LEFT_RIGHT.value   :
				ki_left_right -= KI_LEFT_RIGHT_INC
			elif debug_mode == DEBUG_MODES.VELOCITY.value :
				vel_fwd_rev -= VEL_INC		
					
		#Publish values
		k_values_msg.ki_fwd_rev    = ki_fwd_rev
		k_values_msg.kp_fwd_rev    = kp_fwd_rev
		k_values_msg.ki_left_right = ki_left_right
		k_values_msg.kp_left_right = kp_left_right
		k_values_msg.vel_fwd_rev   = vel_fwd_rev
		k_values_msg.vel_left_right = vel_left_right
		#k_values_pub.publish(k_values_msg)
		#print "ki_fwd_rev = {}".format(ki_fwd_rev) 	
		#print "kp_fwd_rev = {}".format(kp_fwd_rev) 	
		#print "ki_left_right = {}".format(ki_left_right) 	
		#print "kp_left_right = {}\n".format(kp_left_right) 	
		#print "vel_fwd_rev = {}\n".format(vel_fwd_rev) 	
		#print "vel_left_right = {}\n".format(vel_left_right) 	
					
	#START controls manual vs OpenCV mode
	elif controlId == XboxControl.START.value and value == 1 :
		manual_mode = not manual_mode
		if(manual_mode):
			openCV_msg.manual_mode = manual_mode
			openCV_pub.publish(openCV_msg)
	#Middle XBOX button controls send it mode
	elif controlId == XboxControl.XBOX.value and value == 1 :
		send_it_mode = not send_it_mode
############################################################################################
#Initialize constants
SERVO_DEG_CHANGE = 20
MAX_DEG = 255
MIN_DEG = 0
TURN_VELOCITY_DEC = 20
VEL_INC = 0.25

KP_FWD_REV_INC    = 0.05
KI_FWD_REV_INC    = 0.005
KP_LEFT_RIGHT_INC = 0.0005
KI_LEFT_RIGHT_INC = 0.00005

#Initialize xbox controller
xboxCont = xbox_360_controller_object.XboxController(
    controllerCallBack = handleInput,
    joystickNo = 0,
    deadzone = 25,      # ignore very small joystick offsets
    scale = 60,        # match scale used by motors
    invertYAxis = True  # pushing joystick up now gives positive values
)

#Initialize publishers
motorAPI_pub = rospy.Publisher('xbox_360_controller_motorAPI',
				xbox_360_controller_motorAPI,
				queue_size = 100)

openCV_pub   = rospy.Publisher('xbox_360_controller_openCV',
				xbox_360_controller_openCV,
				queue_size = 100)

k_values_pub   = rospy.Publisher('k_values',
				k_values,
				queue_size = 100)


print "hi"
#Initialize node
rospy.init_node('xbox_360_controller')

#Initialize publisher's message
motorAPI_msg = xbox_360_controller_motorAPI()
openCV_msg   = xbox_360_controller_openCV() 
k_values_msg     = k_values()

#Initialize variables Xbox -> motorAPI
y_cmd_vel      = 0
fwd_vel        = 0
back_vel       = 0
direction      = Directions.STOP.value
r_analog_pos   = 0
pan_servo_deg  = 128
tilt_servo_deg = 128
send_it_mode   = False

#Send values
motorAPI_msg.pan_servo_deg = pan_servo_deg
motorAPI_msg.tilt_servo_deg = tilt_servo_deg
motorAPI_pub.publish(motorAPI_msg)

#Initialize variables Xbox -> openCV
#Variable decides if motorAPI is in manual or OpenCV mode
manual_mode = True
openCV_color = 0

#K_values used for testing autonomous
ki_fwd_rev     = 0.012 
kp_fwd_rev     = 0.1
ki_left_right  = 0.0001
kp_left_right  = 0.005
vel_fwd_rev   = 20
vel_left_right = 20
debug_mode    = DEBUG_MODES.K_FWD_REV.value  

#Initialize 
k_values_msg.ki_fwd_rev = ki_fwd_rev
k_values_msg.kp_fwd_rev = kp_fwd_rev
k_values_msg.ki_left_right = ki_left_right
k_values_msg.kp_left_right = ki_left_right
k_values_msg.vel_fwd_rev = vel_fwd_rev
k_values_msg.vel_left_right = vel_left_right
#Send this initially
#k_values_pub.publish(k_values_msg)

if __name__ == '__main__':
	try:
		xboxCont.start()
		rospy.on_shutdown(exit)
		
	except rospy.ROSInterruptException: pass
