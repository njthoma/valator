#include "valator/bq34z100.h"

// File handle for I2C /dev entry
static int i2c_fh;

//*****************************************************************************
// Use the GPIO_OUT_REG pins connected to the AT23QT2120 reset line to hold the 
// AT23QT2120 in reset by setting the pin low 
//*****************************************************************************
static void bq34z100_assert_reset(void)
{
  
  // read the currnet value of the GPIO_OUT_REG
//  int gpio = ece453_reg_read(GPIO_OUT_REG);
  
  // Clear RST to be 0
//  gpio = gpio & ^GPIO_OUT_CAP_SEN_RST_MASK;

  // Write the value back out 
 // ece453_reg_write(GPIO_OUT_REG, gpio);  
}

//*****************************************************************************
// Use the GPIO port connected to the AT23QT2120 reset line to bring the 
// AT23QT2120 out of reset by setting the pin high
//*****************************************************************************
static void bq34z100_deassert_reset(void)
{
  // read the currnet value of the GPIO_OUT_REG
//  int gpio = ece453_reg_read(GPIO_OUT_REG);

  // Set RST to be 1
//  gpio = gpio | GPIO_OUT_CAP_SEN_RST_MASK;
//  sleep(1);
 
  // Write the value back out 
//  ece453_reg_write(GPIO_OUT_REG, gpio);
}

//*****************************************************************************
//*****************************************************************************
static void bq34z100_set_slave_address(void)
{
	if (ioctl(i2c_fh, I2C_SLAVE, BQ34Z100_SLAVE_ADDR) < 0) {
		perror("Error setting slave address");
		exit(1);
	}
}


//*****************************************************************************
//*****************************************************************************
void bq34z100_open(void)
{
	i2c_fh = open(I2C_DEV, O_RDWR);
	if (i2c_fh < 0) {
		perror("i2c_open failed");
		exit(1);
	}

//bq34z100_deassert_reset();

  // Set the slave address for the at42qt2120
  bq34z100_set_slave_address();

}

//*****************************************************************************
//*****************************************************************************
void bq34z100_close(void)
{
  close(i2c_fh);

  // hold the at42qt2120 in reset
  bq34z100_assert_reset();
}

//*****************************************************************************
// Writes one byte of data to the I2C bus.
//
// Note:  The slave address is already set using the at42qt2120_open() function.
//*****************************************************************************
void bq34z100_write(uint8_t reg, uint8_t value)
{
    if(write(i2c_fh, &reg, 1) != 1){
	perror("failed to write reg to battery gauge.");
	exit(1);
    }	
    
    if(write(i2c_fh, &value, 1) != 1) {
	perror("failed to write data to battery gauge");
	exit(1);
    }
}

//*****************************************************************************
// Reads one byte of data from the I2C bus.
//
// Note:  The slave address is already set using the at42qt2120_open() function.
//*****************************************************************************
uint8_t bq34z100_read(uint8_t cmd, uint8_t sub_cmd)
{
    int data;

    if(write(i2c_fh, &cmd, 1) != 1){
	perror("failed to write first byte of command");
	exit(1);
    }

    if(write(i2c_fh, &sub_cmd ,1) != 1){
       perror("failed to write second byte of command");
       exit(1);
    }

    if(read(i2c_fh, &data,1) != 1){
	perror("failed to read from battery gauge");
	exit(1);
    }

    return data;
}
