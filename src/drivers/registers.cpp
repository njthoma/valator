#include "valator/registers.hpp"

int clear_duty_cyc() {
    // clear motor 0
    int ret = ece453_reg_write(DUTY_CYC_M0_REG, 0);
    if (ret < 0) {
        perror("register write error");
        return -1;
    }
    // clear motor 1
    ret = ece453_reg_write(DUTY_CYC_M1_REG, 0);
    if (ret < 0) {
        perror("register write error");
        return -1;
    }
    // clear motor 2
    ret = ece453_reg_write(DUTY_CYC_M2_REG, 0);
    if (ret < 0) {
        perror("register write error");
        return -1;
    }
    // clear motor 3
    ret = ece453_reg_write(DUTY_CYC_M3_REG, 0);
    if (ret < 0) {
        perror("register write error");
        return -1;
    }

    return 0;
}

int set_duty_cyc(int motor, int duty_cyc, bool reverse) {
    char const * reg;

    // duty cycle is 8 bit unsigned integer
    if (duty_cyc < 0 || duty_cyc > 255) {
        perror("duty cycle must be 0-255");
        return -1;
    }

    // bit 9 in duty cycle specifies motor direction (0=fwd,1=rev)
    if (reverse) {
        duty_cyc = (duty_cyc | REVERSE_MOTOR_BIT_MASK);
    }

    // select motor register
    switch (motor) {
        case MOTOR_FL:
            reg = DUTY_CYC_M0_REG;
            break;
        case MOTOR_FR:
            reg = DUTY_CYC_M1_REG;
            break;
        case MOTOR_RL:
            reg = DUTY_CYC_M2_REG;
            break;
        case MOTOR_RR:
            reg = DUTY_CYC_M3_REG;
            break;
        default:
            perror("motor register not found");
            return -1;
    }

    // write to register
    int ret = ece453_reg_write(reg, duty_cyc);
    if (ret < 0) {
        perror("register write error");
        return -1;
    }

    return ret;
}

int get_duty_cyc(int motor) {
    char const * reg;
    int duty_cyc;

    // select motor register
    switch (motor) {
        case MOTOR_FL:
            reg = DUTY_CYC_M0_REG;
            break;
        case MOTOR_FR:
            reg = DUTY_CYC_M1_REG;
            break;
        case MOTOR_RL:
            reg = DUTY_CYC_M2_REG;
            break;
        case MOTOR_RR:
            reg = DUTY_CYC_M3_REG;
            break;
        default:
            perror("motor register not found");
            return -1;
    }

    // read from register
    int ret = ece453_reg_read(reg);
    if (ret < 0) {
        perror("register read error");
        return -1;
    }

    // clear motor direction bit
    duty_cyc = (ret & ~(REVERSE_MOTOR_BIT_MASK));

    return duty_cyc;
}

int get_enc_counts(int encoder) {
    char const * reg;

    // select motor register
    switch (encoder) {
        case ENCODER_FL:
            reg = ENC_COUNT_M0_REG;
            break;
        case ENCODER_FR:
            reg = ENC_COUNT_M1_REG;
            break;
        case ENCODER_RL:
            reg = ENC_COUNT_M2_REG;
            break;
        case ENCODER_RR:
            reg = ENC_COUNT_M3_REG;
            break;
        default:
            perror("encoder register not found");
            return -1;
    }

    // read from register
    int ret = ece453_reg_read(reg);

    return ret;
}

int get_angle(int servo) {
    const char * reg;

    // select servo register
    switch (servo) {
        case SERVO_PAN:
            reg = POSITION_S0_REG;
            break;
        case SERVO_TILT:
            reg = POSITION_S1_REG;
            break;
        default:
            perror("servo register not found");
            return -1;
    }

    // read from register
    int ret = ece453_reg_read(reg);

    return ret;
}

int set_angle(int servo, int angle) {
    const char * reg;

    // select servo register
    switch (servo) {
        case SERVO_PAN:
            reg = POSITION_S0_REG;
            break;
        case SERVO_TILT:
            reg = POSITION_S1_REG;
            break;
        default:
            perror("servo register not found");
            return -1;
    }

    // write to register
    int ret = ece453_reg_write(reg, angle);
    if (ret < 0) {
        perror("register write error");
        return -1;
    }

    return ret;
}
